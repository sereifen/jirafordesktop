﻿using Newtonsoft.Json;

namespace IssueManager.Entities.Misc
{
    public class File
    {
        [JsonProperty("self")]
        public string Self { get; set; }

        [JsonProperty("filename")]
        public string Filename { get; set; }
    }
}