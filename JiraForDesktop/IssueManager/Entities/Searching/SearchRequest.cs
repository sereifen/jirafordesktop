﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace IssueManager.Entities.Searching
{
    /// <summary>
    /// A class representing a JIRA REST search request
    /// </summary>
    public class SearchRequest
    {
        public SearchRequest()
        {
            Fields = new List<string>();
        }

        [JsonProperty("jql")]
        public string JQL { get; set; }

        [JsonProperty("startAt")]
        public int StartAt { get; set; }

        [JsonProperty("maxResults")]
        public int MaxResults { get; set; }

        [JsonProperty("fields")]
        public List<string> Fields { get; set; }
    }
}