﻿using JiraExample.Entities;
using Newtonsoft.Json;

namespace IssueManager.Entities.Issues
{
    public class Status : BaseEntity
    {
        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("iconUrl")]
        public string IconUrl { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }
    }
}