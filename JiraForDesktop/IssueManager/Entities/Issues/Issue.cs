﻿using JiraExample.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using IssueManager.Entities.Projects;

namespace IssueManager.Entities.Issues
{
    /// <summary>
    /// A class representing a JIRA issue
    /// </summary>
    public class Issue : BaseEntity
    {
        private string _mKeyString;

        public Issue()
        {
            Fields = new Fields();
        }

        [JsonProperty("expand")]
        public string Expand { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        #region Special key solution

        [JsonProperty("key")]
        public string ProxyKey
        {
            get
            {
                return Key.ToString();
            }
            set
            {
                _mKeyString = value;
            }
        }

        [JsonIgnore]
        public IssueKey Key
        {
            get
            {
                return IssueKey.Parse(_mKeyString);
            }
        }

        #endregion Special key solution

        [JsonProperty("fields")]
        public Fields Fields { get; set; }

        /// <summary>
        /// test all field if i's null and add parameters for check befose send it to jira
        /// </summary>
        public void Compute()
        {
            if (Expand == null)
                Expand = String.Empty;
            if (Fields.Version == null)
                Fields.Version = string.Empty;
            if (Fields.Assignee == null)
                Fields.Assignee = new Assignee();
            if (Fields.Crash == null)
                Fields.Crash = string.Empty;
            if (Fields.Description == null)
                Fields.Description = string.Empty;
            if (Fields.Emails == null)
                Fields.Emails = string.Empty;
            if (Fields.Issuetype == null)
                Fields.Issuetype = new IssueType();
            if (Fields.Languages == null)
                Fields.Languages = string.Empty;
            if (Fields.Project == null)
                Fields.Project = new ProjectDescription();
            if (Fields.Status == null)
                Fields.Status = new Status();
            if (Fields.Summary == null)
                Fields.Summary = string.Empty;
            if (Fields.Versions == null)
                Fields.Versions = new List<Version>();
        }
    }
}