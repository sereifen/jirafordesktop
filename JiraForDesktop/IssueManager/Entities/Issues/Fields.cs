﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using IssueManager.Entities.Projects;

namespace IssueManager.Entities.Issues
{
    public class Fields
    {
        public Fields()
        {
            Status = new Status();
            Assignee = new Assignee();
            Project = new ProjectDescription();
            Issuetype = new IssueType();
            Versions = new List<Version>();
        }

        [JsonProperty("summary")]
        public string Summary { get; set; }

        [JsonProperty("status")]
        public Status Status { get; set; }

        [JsonProperty("assignee")]
        public Assignee Assignee { get; set; }

        [JsonProperty("project")]
        public ProjectDescription Project { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("issuetype")]
        public IssueType Issuetype { get; set; }

        [JsonProperty("customfield_10200")]
        public string Emails { get; set; }

        [JsonProperty("customfield_10300")]
        public string Version { get; set; }

        [JsonProperty("customfield_10302")]
        public string Languages { get; set; }

        [JsonProperty("customfield_10301")]
        public string Crash { get; set; }

        [JsonProperty("fixVersions")]
        public IEnumerable<Version> Versions { get; set; }

        public string GetSerializerUpdate()
        {
            var ret = @"{ ""fields"" :{ " + GetCustomfields() + " }}";
            ret = ret.Replace("\r", "\\r");
            ret = ret.Replace("\n", "\\n");
            return ret;
        }

        public string GetSerializeNew()
        {
            var ret = @"{ ""fields"" :{ ""summary"" : """ + Summary + @""", ""project"" : " + JsonConvert.SerializeObject(Project) + @", ""fixVersions"" : " + JsonConvert.SerializeObject(Versions) + @", ""issuetype"" : " + JsonConvert.SerializeObject(Issuetype) + @", ""description"" : """ + Description + @""", " + GetCustomfields() + " }}";
            //var ret = @"{ ""fields"" :{ ""summary"" : """ + Summary + @""", ""project"" : " + JsonConvert.SerializeObject(Project) + @", ""fixVersions"" : [{ ""id"" : """ + Versions.ToArray()[0].Id + @"""}], ""issuetype"" : " + JsonConvert.SerializeObject(Issuetype) + @", ""description"" : """ + Description + @""", " + GetCustomfields() + " }}";
            ret = ret.Replace("\r", "\\r");
            ret = ret.Replace("\n", "\\n");
            return ret;
        }

        private string GetCustomfields()
        {
            return @"""customfield_10200"" : """ + Emails + @""", ""customfield_10300"" : """ + Version + @""", ""customfield_10302"" : """ + Languages + @""", ""customfield_10301"" : """ + Crash + @"""";
        }
    }
}