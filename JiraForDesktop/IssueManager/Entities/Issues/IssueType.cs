﻿using Newtonsoft.Json;

namespace IssueManager.Entities.Issues
{
    public class IssueType
    {
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}