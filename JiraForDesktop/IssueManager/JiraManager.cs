﻿using IssueManager.Entities.Issues;
using IssueManager.Entities.Projects;
using IssueManager.Entities.Searching;
using JiraExample;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Version = IssueManager.Entities.Issues.Version;

namespace IssueManager
{
    public class JiraManager
    {
        public string Error;
        private readonly string _mBaseUrl;
        private readonly string _mUsername;
        private readonly string _mPassword;

        public Dictionary<ProjectDescription, List<Issue>> JiraDictionary { get; private set; }
        public Dictionary<ProjectDescription, List<Entities.Issues.Version>> JiraDictionaryVersions { get; private set; }

        /// <summary>
        /// Constructor for jira manager
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        public JiraManager(string username, string password,string url)
        {
            _mUsername = username;
            _mPassword = password;
            _mBaseUrl = url;
        }

        /// <summary>
        /// Get all database from Jira (project, issues and versions)
        /// </summary>
        public void GetDataBase()
        {
            JiraDictionary = new Dictionary<ProjectDescription, List<Issue>>();
            JiraDictionaryVersions = new Dictionary<ProjectDescription, List<Version>>();
            var projects = GetProjects();
            foreach (var project in projects)
            {
                string jql = "project = " + project.Key;
                var issues = GetIssues(jql);
                JiraDictionary.Add(project, issues);
                var ver = GetVersions(project.Key);
                JiraDictionaryVersions.Add(project, ver);
            }
        }

        /// <summary>
        /// Add a issue to jira if not exist and add a coment with mail
        /// </summary>
        /// <param name="issue">Issue to add or actualize</param>
        /// <param name="actualize">if it's true actualize the issue on jira</param>
        public void AddIssue(Issue issue, bool actualize)
        {
            Error = String.Empty;
            CreateIssue(issue, actualize);
        }
        
        /// <summary>
        /// Get the versions from jira
        /// </summary>
        /// <param name="keyproject">Key from project</param>
        /// <returns></returns>
        private List<Entities.Issues.Version> GetVersions(string keyproject)
        {
            List<Entities.Issues.Version> versions;
            string projectKey = keyproject;
            try
            {
                var result = RunQuery(JiraResource.project, projectKey + "/versions");
                versions = JsonConvert.DeserializeObject<List<Entities.Issues.Version>>(result);
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
            return versions;
        }

        /// <summary>
        /// Add a comment on issue.ProxyKey make shure that exist before call it
        /// </summary>
        /// <param name="issue">issue where want add the comment</param>
        /// <param name="comment">comment to add</param>
        /// <returns>return false on Error</returns>
        public bool AddComment(Issue issue, string comment)
        {
            string data = @"{ ""body"": """ + comment + @""" }";
            try
            {
                RunQuery(JiraResource.issue, issue.ProxyKey + "/comment", data, "POST");
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
            return true;
        }
        
        /// <summary>
        /// Get all projects in case of return null check Error
        /// </summary>
        /// <returns>list of projects on jira</returns>
        public List<ProjectDescription> GetProjects()
        {
            Error = String.Empty;
            try
            {
                string projectsString = RunQuery(JiraResource.project);
                return JsonConvert.DeserializeObject<List<ProjectDescription>>(projectsString);
            }
            catch (Exception ex)
            {
                Error = ex.Message;
            }
            return null;
        }

        /// <summary>
        /// Runs a query towards the JIRA REST api
        /// </summary>
        /// <param name="resource">The kind of resource to ask for</param>
        /// <param name="argument">Any argument that needs to be passed, such as a project key</param>
        /// <param name="data">More advanced data sent in POST requests</param>
        /// <param name="method">Either GET or POST</param>
        /// <returns>return the response from Jira</returns>
        private string RunQuery(
            JiraResource resource,
            string argument = null,
            string data = null,
            string method = "GET")
        {
            string url = string.Format("{0}{1}/", _mBaseUrl, resource.ToString());

            if (argument != null)
            {
                url = string.Format("{0}{1}/", url, argument);
            }

            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            request.ContentType = "application/json";
            request.Method = method;

            if (data != null)
            {
                using (StreamWriter writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(data);
                }
            }

            string base64Credentials = GetEncodedCredentials();
            request.Headers.Add("Authorization", "Basic " + base64Credentials);

            HttpWebResponse response = request.GetResponse() as HttpWebResponse;

            string result = string.Empty;
            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                result = reader.ReadToEnd();
            }

            return result;
        }
        
        /// <summary>
        /// Create a Issue
        /// </summary>
        /// <param name="issue">Issue to add to jira</param>
        /// <param name="update">trye for update the issue, false create new</param>
        private void CreateIssue(Issue issue, bool update)
        {
            issue.Compute();
            string data;
            if (update)
                data = issue.Fields.GetSerializerUpdate();
            else
                data = issue.Fields.GetSerializeNew();

            try
            {
                if (update)
                    RunQuery(JiraResource.issue, issue.ProxyKey, data, "PUT");
                else
                    RunQuery(JiraResource.issue, data: data, method: "POST");
            }
            catch (Exception ex)
            {
                Error = ex.Message;
            }
        }

        /// <summary>
        /// Get all isues for jql
        /// </summary>
        /// <param name="jql">jql for filter</param>
        /// <param name="fields">selected parameters only get</param>
        /// <param name="startAt">start index</param>
        /// <param name="maxResult">max index</param>
        /// <returns></returns>
        private List<Issue> GetIssues(
                    string jql,
                    List<string> fields = null,
                    int startAt = 0,
                    int maxResult = 5000)
        {
            fields = fields ?? new List<string> { "summary", "issuetype", "status", "project", "assignee", "description", "versions", "customfield_10200", "customfield_10300", "customfield_10302", "customfield_10301" };

            SearchRequest request = new SearchRequest();
            request.Fields = fields;
            request.JQL = jql;
            request.MaxResults = maxResult;
            request.StartAt = startAt;

            string data = JsonConvert.SerializeObject(request);
            string result = RunQuery(JiraResource.search, data: data, method: "POST");

            SearchResponse response = JsonConvert.DeserializeObject<SearchResponse>(result);

            return response.IssueDescriptions;
        }

        /// <summary>
        /// Encode the credential for actual User
        /// </summary>
        /// <returns>Encoded credential for selected User</returns>
        private string GetEncodedCredentials()
        {
            string mergedCredentials = String.Format("{0}:{1}", _mUsername, _mPassword);
            byte[] byteCredentials = Encoding.UTF8.GetBytes(mergedCredentials);
            return Convert.ToBase64String(byteCredentials);
        }
    }
}