﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using IssueManager;
using IssueManager.Entities.Projects;

namespace JiraForDesktop
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private JiraManager _jira;

        public MainWindow()
        {
            InitializeComponent();
            Loaded += MyWindow_Loaded;
        }

        private void buttonSend_Click(object sender, RoutedEventArgs e)
        {
            //TODO CREATE ISSUE AND SEND IT TO JIRA
        }

        private void MyWindow_Loaded(object sender, RoutedEventArgs e)
        {
            var bw = new BackgroundWorker();
            bw.DoWork += (o, args) =>
            {
                _jira = new JiraManager("", "", "");//TODO ACTUALIZE 
                _jira.GetDataBase();
            };
            bw.RunWorkerCompleted +=BwOnRunWorkerCompleted;
            bw.RunWorkerAsync();
        }

        private void BwOnRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs runWorkerCompletedEventArgs)
        {
            ActualizeUi();
        }

        private void ActualizeUi()
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(ActualizeUi);
                return;
            }
            if (_jira.JiraDictionary==null) return;
            foreach (var tupla in _jira.JiraDictionary)
            {
                comboBoxProjects.Items.Add(new ComboBoxItem() {Name = tupla.Key.Name,Tag = tupla.Key});
            }
            
        }

        private void comboBoxProjects_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_jira.JiraDictionaryVersions == null) return;
            var tag = (comboBoxProjects.SelectedItem as ComboBoxItem)?.Tag as ProjectDescription;
            if (tag == null) return;
            comboBoxVersion.Items.Clear();
            foreach (var version in _jira.JiraDictionaryVersions[tag])
            {
                comboBoxVersion.Items.Add(new ComboBoxItem() { Name = version.Name, Tag = version});
            }
        }
    }
}
